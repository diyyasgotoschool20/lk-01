package com.company;
public class Kereta {
    // Tulis kode disini
    private String NamaKereta;
    private int jumlahTiket;
    private Ticket[] ticket;
    public Kereta(){
        this.NamaKereta = "Komuter";
        this.jumlahTiket = 1000;
        ticket = new Ticket[this.jumlahTiket];
    }
    public Kereta(String namaKereta, int jumlahTiket){
        this.NamaKereta = namaKereta;
        this.jumlahTiket = jumlahTiket;
        ticket = new Ticket[this.jumlahTiket];
    }
    public void tambahTiket(String nama){
        if (jumlahTiket > 0) {
            this.jumlahTiket--;
            for (int i = 0; i < ticket.length; i++) {
                if (ticket[i] != null) {
                    continue;
                }
                else {
                    ticket[i] = new Ticket(nama);
                    break;
                }
            }
            System.out.println("================================================================");
            System.out.print("Tiket berhasil dipesan");

            if (jumlahTiket < 30 && jumlahTiket >= 0) {
                System.out.print(". Jumlah tiket teresdia: " +jumlahTiket);
            }
            System.out.println();
        }
        else {
            System.out.println("================================================================");
            System.out.println("Kereta telah habis dipesan, silahkan cari jadwal keberangkatan lainnya");
        }
    }
    public void tambahTiket(String nama, String asal, String tujuan){
        if (jumlahTiket > 0) {
            this.jumlahTiket--;
            for (int i = 0; i < ticket.length; i++) {
                if (ticket[i] != null) {
                    continue;
                }
                else {
                    ticket[i] = new Ticket(nama, asal, tujuan);
                    break;
                }
            }

            System.out.println("================================================================");
            System.out.print("Tiket berhasil dipesan");
            if (jumlahTiket < 30 && jumlahTiket >= 0) {
                System.out.print(". Jumlah tiket teresdia: " +jumlahTiket);
            }
            System.out.println();
        }
        else {
            System.out.println("================================================================");
            System.out.println("Kereta telah habis dipesan, silahkan cari jadwal keberangkatan lainnya");
        }
    }
    public void tampilkanTiket(){
        System.out.println("================================================================");
        System.out.println("Daftar penumpang kereta api " +this.NamaKereta +":");
        System.out.println("--------------------------");
        for (Ticket tiket : ticket) {
            if (tiket == null) {
                break;
            }
            else {
                tiket.printTicket();
            }
        }

    }

}
